extern crate getopts;
extern crate prestodb_rust;
extern crate csv;
use prestodb_rust::connect;
use std::env;
use std::error::Error;
use std::fs::File;
use std::fs::OpenOptions;
use std::path::Path;
use std::io::Read;
use csv::WriterBuilder;
use std::io::BufWriter;

fn main() {
    let args : Vec<String> = env::args().collect();
    let mut opts = getopts::Options::new();
    opts.optopt("u", "user", "Username shown next to query", "USER");
    opts.optopt("p", "password", "Password for protected servers, must use -ssl with this.", "******");
    opts.optopt("q", "query", "Specifies written query, use qf instead to read from file", "SELECT * FROM TABLE");
    opts.optopt("c", "cluster", "Specifies cluster URL", "URL");
    opts.optopt("f", "queryfile", "Specifies a file to read a query from", "FILE");
    opts.optopt("", "outputfile", "Specifies a file to write the query results to", "FILE");
    opts.optopt("", "catalog", "Specifies the catalog of the prestodb cluster", "hive");
    opts.optopt("", "schema", "Specifies the schema of the prestodb cluster", "default");
    opts.optflag("s", "showoutput", "Print the query output to display");
    opts.optflag("", "ssl", "Is SSL/HTTPS needed for query?");
    opts.optflag("h", "help", "Show this information");
    
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => {m}, 
        Err(f) => {panic!(f.to_string());}
    };
    if matches.opt_present("h") {print_help(); return;}
    
    if matches.opt_present("u") == false || matches.opt_present("c") == false { println!("Requires a username and cluster to query"); return;}
    if matches.opt_present("p") & (matches.opt_present("ssl") == false) {println!("SSL must be enabled to send a password, use -ssl"); return;}

    let query = match matches.opt_present("q") {
        true => match matches.opt_present("f") {
            true => {println!("Please enter a query or a query file, not both"); return;},
            false => {matches.opt_str("q").unwrap()}
        }, 
        false => match matches.opt_present("f"){
            true => {read_query_from_file(matches.opt_str("f").unwrap())},
            false => {println!("Specify either a query or a query file"); return;}
        }
    };

    let presto_connector = connect::Connector::new(
        matches.opt_str("c").unwrap(), // URL
        matches.opt_str("u").unwrap(), // User
        matches.opt_str("schema").unwrap(), // Schema
        matches.opt_str("catalog").unwrap(), // Catalog
        matches.opt_present("ssl") // SSL
    );

    let mut presto_connector = presto_connector.parse_uri().unwrap();
    let presto_response = presto_connector
        .send_query(String::from(
            query
        ))
        .unwrap();
        
    let full_response = &presto_connector.parse_query_response(presto_response);
    
    if matches.opt_present("outputfile") {
        match write_query_to_file(matches.opt_str("outputfile").unwrap(),
                            full_response.data_set_iter.clone().unwrap(),
                            full_response.columns.clone()
                                .unwrap()
                                .iter()
                                .map(|ColumnData| ColumnData.name.to_string())
                                .collect::<Vec<String>>()
        ) {
            Ok(_) => println!("Successfully wrote to {}", matches.opt_str("outputfile").unwrap()), 
            Err(e) => println!("{} \n Failed to write to {}", e, matches.opt_str("outputfile").unwrap())
        }
    }
    if matches.opt_present("s"){
        println!("{:?}", full_response.columns.clone()
                                .unwrap()
                                .iter()
                                .map(|ColumnData| ColumnData.name.to_string())
                                .collect::<Vec<String>>());
        println!("Data is {:?}", &full_response.data_set_iter.clone().unwrap().dataset);
    }
}


fn read_query_from_file(file_name: String) -> String {
    let mut file = File::open(file_name)
        .expect("File not found");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Error on file read");
    contents
}

fn write_query_to_file(file_name: String,
                       write_data: prestodb_rust::connect::DataSetIter,
                       formatted_header_object: Vec<String>) -> Result<(), Box<Error>>{
    let path = Path::new(&file_name);
    let mut wtr;
    wtr = WriterBuilder::new().from_writer(BufWriter::new(File::create(path)?));
    wtr.write_record(formatted_header_object)?;
    for line in write_data{
        wtr.serialize(line)?;
    }
    wtr.flush();
    Ok(())
}

fn print_help(){
    println!("Send a query to a PrestoDB SQL Server");
    println!("    -h, --help           : show this information");
    println!("    -u, --user           : username");
    println!("    -p, --password       : password for secure servers, requires -ssl to be used");
    println!("    -q, --query          : query to be passed");
    println!("    -c, --cluster        : cluster to query");
    println!("    -f, --queryfile      : queryfile to read from - use instead of query");
    println!("    -s, --showoutput     : Print the query output to display");
    println!("      , --outputfile     : file to write query output to as csv");
    println!("      , --catalog        : catalog of cluster");
    println!("      , --schema         : schema of cluster");
    println!("      , --ssl            : if https is required for cluster");
}
