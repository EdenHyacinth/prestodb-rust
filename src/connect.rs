#![allow(unused_imports)]
#![allow(unused_variables)]
use futures::{Future, Stream, IntoFuture, future};
use hyper::error;
use hyper::Uri;
use hyper::Chunk;
use hyper::{Client, Method, Request};
use hyper::header::{ContentLength, ContentType};
use hyper::header::{Authorization, Accept, Headers, qitem};
use tokio_core::reactor::Core;
use serde_json::Value;
use serde_json;
use std::{fmt, mem, io, string};
use std::str::FromStr;
use std::{thread, time};
use std::ops::Add;
use regex::Regex;
use serde::de::{self, Deserialize, Deserializer, Visitor};
use std::io::Error;
use hyper_tls::HttpsConnector;
/// Header for Presto User Field
header! { (XPrestoUser, "X-Presto-User") => [String] }
/// Header to specify Presto Catalog
header! { (XPrestoCatalog, "X-Presto-Catalog") => [String] }
/// Header to specify Presto Schema
header! { (XPrestoSchema, "X-Presto-Schema") => [String] }

/// Default connector to Presto
/// 
#[derive(Debug)]
pub struct Connector {
    /// URL of Presto Cluster, needs to contain port.
    url: String,
    /// User attached to the query.
    user: String,
    /// Schema of the server, default is default
    schema: String,
    /// Catalog of the server, default is hive or native. 
    catalog: String,
     /// Whether to use a HTTPS address or not, will handle most SSL. 
    ssl: bool,
}

impl Connector {
    pub fn new(url: String, user: String, schema: String, catalog: String, ssl: bool) -> Connector {
        Connector {
            url: url,
            user: user,
            schema: schema, 
            catalog: catalog, 
            ssl : ssl,
        }
    }
}

impl Default for Connector {
    fn default() -> Connector {
        Connector {
            url: String::from("localhost:8080"),
            user: String::from("unset"),
            schema: String::from("default"),
            catalog: String::from("native"),
            ssl: false,
        }
    }
}

#[derive(Clone, Debug)]
pub struct ParsedUrl(Uri);

/// Determine if query has finished 
/// 
/// PrestoDB will continuously send a nextUri field until the query is finished. 
/// This enum will either hold a ParsedUrl, which is regex matched against the 
/// nextUri that Presto will send, or will hold None, to indicate the query is finished. 
/// 
/// This field, including the ParsedUrl it contains, is automatically deserialised from the json response. 
#[derive(Clone, Debug)]
pub enum QueryResponse {
    None,
    Some(ParsedUrl),
}

impl<'de> Deserialize<'de> for QueryResponse {
    fn deserialize<D>(deserializer: D) -> Result<QueryResponse, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(URIVisitor)
    }
}

struct URIVisitor;

impl<'de> Visitor<'de> for URIVisitor {
    type Value = QueryResponse;
    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a URI-type string that can be parsed")
    }
    // This probably fails if you give it a str that can't be a string.
    fn visit_str<E>(self, visitor: &str) -> Result<QueryResponse, E>
    where
        E: de::Error,
    {
        let re = Regex::new(r"v1/statement/[0-9_a-z]+/[0-9]{1,4}").unwrap();
        match visitor.parse::<Uri>() {
            Ok(value) => {
                match re.is_match(value.path()) {
                    true => Ok(QueryResponse::Some(ParsedUrl(value))),
                    false => Ok(QueryResponse::None),
                }
            }
            Err(e) => Err(E::custom(format!("URL doesn't parse as URI: {}", visitor))),
        }
    }
}

/// Initial Reply Structure from Presto 
#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct QueryResults {
    id: String,
    info_uri: String,
    next_uri: QueryResponse,
    stats: PrestoStats,
}

/// Structure of SELECT Query reply from Presto
/// 
/// Contains more fields than a QueryResults as it has to contain
/// the data being returned, as well as a handler for that data. 
#[derive(Clone, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct PrestoRunning {
    id: String,
    info_uri: String,
    partial_cancel_uri: Option<String>,
    next_uri: Option<QueryResponse>,
    pub columns: Option<Vec<ColumnData>>,
    pub data: Option<Vec<Value>>,
    #[serde(skip)]
    formatted_data: Option<DataSet>,
    #[serde(skip)]
    // Leaving this on to use while we're joining the query.
    pub data_set_iter: Option<DataSetIter>,
    stats: PrestoStats,
}

#[derive(Clone, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct PrestoStats {
    cpu_time_millis: u32,
    nodes: u32,
    processed_bytes: u32,
    processed_rows: u32,
    queued_splits: u32,
    running_splits: u32,
    completed_splits: Option<u32>,
    scheduled: bool,
    state: String,
    total_splits: u32,
    user_time_millis: u32,
    wall_time_millis: u32,
    root_stage: Option<PrestoRootStage>,
}

#[derive(Clone, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct PrestoRootStage {
    stage_id: String,
    state: String,
    done: bool,
    nodes: u32,
    total_splits: u32,
    queued_splits: u32,
    running_splits: u32,
    completed_splits: u32,
    user_time_millis: u32,
    cpu_time_millis: u32,
    wall_time_millis: u32,
    processed_rows: u32,
    processed_bytes: u32,
    sub_stages: Vec<Option<PrestoRootStage>>,
}

impl PrestoRunning {
    /// Ping Presto & Retrieve the Next JSON of Data
    fn update_status(mut self) -> future::FutureResult<Self, ::std::io::Error> {
        let mut core = Core::new().unwrap();
        let client = Client::new(&core.handle());
        let next_uri: Uri = match self.next_uri.clone() {
            Some(value) => {
                match value {
                    QueryResponse::Some(uri) => uri.0, 
                    QueryResponse::None => panic!("No URI Mapping"),
                }
            } 
            // If there's no URI Mapping, it means the query failed. 
            // So this is a good place to add in the error code. 
            None => panic!("No URI Mapping"),
        };
        let req = Request::new(Method::Get, next_uri);
        let initial_request = client.request(req).and_then(|res| {
            res.body().concat2().and_then(move |body: Chunk| {
                let initial_data_shape: PrestoRunning = serde_json::from_slice(&body).unwrap();
                //println!("Presto Running: {:#?}", initial_data_shape);
                future::ok(initial_data_shape)
            })
        });
        let reply = core.run(initial_request).unwrap();

        // Situation 1:  Self - Data, Reply - Data
        if let Some(mut i) = self.data_set_iter.clone() {
            if let Some(j) = reply.data.clone() {         
                self.data_set_iter = Some({i.extend(j);i});
            }
        } else if let Some(i) = reply.data.clone() {
            // Situation 2: Self - None, Reply - Data
            // Initialise a DataSetIter on the reply, and transfer it to the Self. 
            self.data = reply.data.clone();
            self.data_set_iter = match reply.data.clone() {
                Some(data) => {
                    let cols = reply.columns.clone().unwrap()[..].len();
                    let rows = data[..].len();
                    self.formatted_data = Some(DataSet::new(
                                data[..].len(), 
                                cols, 
                                data
                            ));
                    Some(DataSetIter::new(
                            self.formatted_data.clone().unwrap()
                        ))
                },
                None => None};
            self.columns = reply.columns;
        };
        // Situation 3: Self - Data, Reply - None
        // We don't need to do anything here, just let the self continue.
        self.stats.root_stage = match reply.stats.root_stage.clone() {
            Some(root_stage) => Some(root_stage), 
            None => self.stats.root_stage
        };

        // If there is data, and there's no Iterator initialised,
        // initialise an iterator. 
        self.data_set_iter = match self.data_set_iter {
            Some(iterator) => Some(iterator),
            None => match self.data.clone() {
                Some(data) => {
                    let cols = self.columns.clone().unwrap()[..].len();
                    let rows = data[..].len();
                    self.formatted_data = Some(DataSet::new(
                                data[..].len(), 
                                cols, 
                                data
                            ));
                    Some(DataSetIter::new(
                            self.formatted_data.clone().unwrap()
                        ))
                },
                None => None
            } 
        };
        self.next_uri = reply.next_uri;
        future::ok(self)
    }
}
  
// Reimplementing the DataSet through a single vector implementation. 
/// Structure to store Presto Data Response
/// 
/// Handled by DataSetIter, cann be indexed as a 2D Vector 
/// using std::ops::Index[(usize,usize)], or indexed directly
/// using DataSet.data[(usize)]. Contains two usizes referencing 
/// the size of the dataset in rows and cols. 
#[derive(Serialize, Deserialize, Clone)]
pub struct DataSet{
    rows: usize, 
    cols: usize,
    pub data: Vec<Value>
}


impl DataSet {
    fn new(rows: usize, cols: usize, data: Vec<Value>) -> DataSet {
        DataSet {
            rows: rows, 
            cols: cols, 
            data: data
        }
    }
}

impl ::std::ops::Index<(usize, usize)> for DataSet{
    type Output = Value; 
    fn index(&self, idx: (usize, usize)) -> &Value {
        assert!(idx.0 < self.cols && idx.1 < self.rows);
        &self.data[idx.0 * self.cols + idx.1]
    }
}

/// Iterable Object for DataSet
/// 
/// Simple handler with an attached dataset and a usize to iterate through rows. 
#[derive(Debug, Clone)]
pub struct DataSetIter {
    pub dataset: DataSet, 
    cur: usize
}

impl DataSetIter {
    fn new(d: DataSet) -> DataSetIter {
        DataSetIter {
            dataset: d, 
            cur: 0
        }
    }
}

impl Iterator for DataSetIter {
    type Item = Value;
    fn next(&mut self) -> Option<Value> {
       if let Some(first) = self.dataset.data[..].get(self.cur) {
            self.cur += 1;
        // In testing could cheat this as i32 has the CLONE trait. 
        // Unfortunately Value does not, so I have to clone. 
        // Or I could just mem::replace and move it out, but technically I want 
        // to keep it? 
            Some(first.clone())
        } else {
            None
        }
    }
}

impl Extend<Value> for DataSetIter{
    fn extend<T: IntoIterator<Item=Value>>(&mut self, iter:T) {
        let mut t_count = 0;
        for elem_row in iter {
            t_count += 1;
            self.dataset.add(elem_row)
        }
        self.dataset.rows += t_count
    }
}

impl DataSet{
    fn add(&mut self, elem: Value){
        self.data.push(elem);
    }
}

impl fmt::Debug for DataSet {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "\n");
        for x in 0..self.rows{
            //let max_size = ((x + 1) * self.cols) as usize;
            //let min_size = x * self.cols as usize;
            write!(f, "Dataset Row {:?}\n",
                //&self.data[min_size..max_size]
                &self.data[x]
                );
            }
        write!(f, "DataSet Size: {}x{}", self.cols, self.rows)
    }
}

/// Column Information from Presto SELECT Query Reply
#[derive(Clone, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ColumnData {
    /// Column Name
    pub name: String,
    #[serde(rename = "type")]
    column_type: String,
    type_signature: TypeSignature,
}

#[derive(Clone, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct TypeSignature {
    raw_type: String,
    type_arguments: Option<Vec<ArgumentSignature>>,
    literal_arguments: Option<Vec<ArgumentSignature>>,
    arguments: Option<Vec<ArgumentSignature>>,
}

#[derive(Clone, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct ArgumentSignature {
    kind: String,
    value: i32,
}

impl Connector {
    // Going to expand this for statement checks etc and any PreProc.
    // Currently doesn't do a great deal - it just parses a URL, but I can do that in
    // send query without the issue. It's good for checking some things and adding on the http
    // and stuff.
    /// Parses the URL from a Connector  
    /// 
    /// This is used to format the URL into a suitable format for Presto, such as 
    /// adding on the /v1/statement suffix and ensuring the right type of HTTP is used. 
    /// Realistically nothing changes based on the HTTP status, this package uses
    /// the hyper implementation with TLS/HTTPS as it can handle both, it's just the 
    /// URL the query is sent to. 
    /// 
    /// # Panics
    /// Will panic on receiving a URL that cannot be formatted into a hyper::uri. 
    pub fn parse_uri(mut self) -> Result<Connector, Box<string::ParseError>> {
        let re = Regex::new(r"h?t?t?p?s?:?/?/?w?w?w?([^/]+)").unwrap();
        let base_url = re.captures(&self.url.to_owned()).unwrap()[1].to_owned();
        let url = match self.ssl {
            true => "https://".to_owned() + &base_url + &"/v1/statement".to_owned(), 
            false => "http://".to_owned() + &base_url + &"/v1/statement".to_owned()
            };
        self.url = url.parse()?;
        Ok(self)
    }
    // This is the main bulk of the query so far - it kinda does everything.
    // It should be able to accept a query and a user, ping a server, and retrieve a reply.
    // I guess it should return the query when you fire it?
    // This also doubles for sending non-GET queries, like ALTER/CREATE, where a Vec<Vec<X>>
    // answer wouldn't be appropriate.
    /// Send initial query to Presto
    /// 
    /// This is suitable for non-SELECT queries, but for SELECT queries should be followed up
    /// using parse_query_response to retrieve all the resultant data. 
    /// 
    /// # Errors
    /// Will return an error with hyper::error::UriError if there is a connection fault. 
    pub fn send_query(&mut self, json_query: String) -> Result<QueryResults, Box<error::UriError>> {
        let mut core = Core::new().unwrap();
        let client = Client::configure()
            .connector(HttpsConnector::new(4, &core.handle()).unwrap())
            .build(&core.handle());

        let uri = self.url.parse().unwrap();
        // The type of request - in this case we need to POST
        let mut req = Request::new(Method::Post, uri);
        req.headers_mut().set(ContentType::json());
        req.headers_mut().set(
            ContentLength(json_query.len() as u64),
        );
        req.headers_mut().set(XPrestoUser(self.user.to_owned()));
        req.headers_mut().set(
            XPrestoCatalog(self.catalog.to_owned()),
        );
        req.headers_mut().set(XPrestoSchema(self.schema.to_owned()));
        req.set_body(json_query);

        let post = client.request(req).and_then(|res| {
            //println!("POST: {}", res.status());
            res.body().concat2().and_then(move |body: Chunk| {
                let presto_response: QueryResults = serde_json::from_slice(&body).unwrap();
                Ok(presto_response)
            })
        });
        match core.run(post) {
            // This returns an object that moves us from initialising the query
            // to actually retrieving the results of the query.
            // This is a good place to stop, check for query errors, then move
            // forward if the query is running.
            Ok(response) => Ok(response), 
            Err(e) => panic!("Query failed to return condition: {}", e),
        }
    }
    /// Retrieves data from Presto Response
    /// 
    /// Sent after send_query, this continuously sends HTTP requests to Presto
    /// until all the data from Presto is received. The data will then be 
    /// available in the response field 'DataSetIter' and 'DataSet'.
    /// 
    /// # Panics 
    /// If Presto returns an unsuitable URL, or serde cannot automatically 
    /// deserialize some of the fields in the response, it will panic. 
    pub fn parse_query_response(self, response: QueryResults) -> PrestoRunning {
        let mut core = Core::new().unwrap();
        let client = Client::new(&core.handle());
        let initial_uri: Uri = match response.next_uri {
            QueryResponse::Some(uri) => uri.0, 
            QueryResponse::None => panic!("No URI Available"),
        };
        let req = Request::new(Method::Get, initial_uri);
        let initial_request = client.request(req).and_then(|res| {
            res.body().concat2().and_then(move |body: Chunk| {
                let initial_data_shape: PrestoRunning = match serde_json::from_slice(&body) {
                    Ok(processed_json) => processed_json, 
                    Err(e) => panic!("Issue with serde: {:#?}", e), 
                };
                Ok(initial_data_shape)
            })
        });
        let initial_data = core.run(initial_request).unwrap();
        let client = future::loop_fn(initial_data, |resp_data| {
            //println!("Initialised Loop");
            resp_data.update_status().and_then(|resp_data| if match resp_data
                .next_uri
                .clone() {
                    Some(uri) => {//sp Data is {:?} \n", resp_data.stats.root_stage);
                     false}, 
                    None => {//println!("Resp Data is {:?} \n", resp_data.stats.root_stage);
                     true}
                }
            {
                Ok(future::Loop::Break(resp_data))
            } else {
                thread::sleep(time::Duration::from_millis(50));
                Ok(future::Loop::Continue(resp_data))
            })
        });
        core.run(client).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn read_and_parse() {
        let mut basic_connect: Connector = Connector {
            url: String::from("localhost:8080"),
            user: String::from("Eden"),
            catalog: String::from("hive"),
            schema: String::from("default"),
            ssl: false,
        };
        basic_connect = basic_connect.parse_uri().unwrap();
        let basic_response = basic_connect
            .send_query(String::from(
                r#"SELECT * FROM (VALUES (1, 'a'), (2, 'b'), (3, 'c')) AS t (id, name)"#,
            ))
            .unwrap();
        println!("Basic Response is {:#?}", basic_response);
        let full_response = &basic_connect.parse_query_response(basic_response);
        let columns = full_response.columns.clone().unwrap();
        println!("Columns are: {:?}", columns.iter().map(|col_data| col_data.name.to_string()).collect::<Vec<String>>());
       let data_reply = full_response.data_set_iter.clone().unwrap();
       //println!("Data Size is Cols: {:?} Rows: {:?} / 100,000 ", full_response.data_set_iter.clone().unwrap().dataset.cols, full_response.data_set_iter.clone().unwrap().dataset.rows);
       println!("Data Reply is {:?}", data_reply);
    }
}

